# paths
PREFIX = /usr/local/
SOPATH = ${PREFIX}/share/dstatus/

# plugins
PLUGINS = avg battery brightness cpu ip memory selinux time volume wifi

# includes and libs
LIBS  = -lX11 -ldl
FLAGS = -DSOPATH=\"${SOPATH}\"

# flags
CFLAGS   = -Wall -Wextra -O3 -march=native -std=gnu99 -g ${FLAGS}
LDFLAGS  = ${LIBS}

# compiler and linker
CC = cc
