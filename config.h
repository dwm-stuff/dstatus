/*
 * Copyright (c) 2012, Patrick Steinhardt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#define NPLUGINS 11

#define STATUS_LENGTH 256

#define AVG_LEN           15
#define BATTERY_FORMAT    "%3i"
#define BATTERY_LEN       4
#define TIME_FORMAT       "%a %d %b %H:%M:%S"
#define TIME_LEN          20
#define CPU_FORMAT        "%02Ld"
#define CPU_LEN           4
#define VOL_FORMAT        "%02ld"
#define VOL_LEN           4
#define MEM_FORMAT        "%3li"
#define MEM_LEN           4
#define MPD_FORMAT        "%t (%a)"
#define MPD_LEN           64
#define BRIGHTNESS_FORMAT "%3i"
#define BRIGHTNESS_LEN    4

#define BATTERY_DEV     "/sys/class/power_supply/BAT0/capacity"
#define WIRELESS_IFACE  "wlp3s0"
#define IP_IFACE        "wlp3s0"
#define BRIGHTNESS_DEV  "intel_backlight"

/* \u231A - Watch */
/* \u231B - Hourglass */

#define FORMAT "[\u231A %t][%a \uF3B5%M%% \uF214 %b%% \uF028 %v%%]%([\uF35C %w %i]%)"
