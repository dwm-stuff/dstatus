/*
 * Copyright (c) 2012-2013, Patrick Steinhardt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <dirent.h>
#include <dlfcn.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <X11/Xlib.h>

#include "config.h"

#define UNUSED(x) (void)(x)

typedef bool (*InitFunction)();
typedef void (*ExitFunction)();
typedef char (*FormatFunction)();
typedef const char *(*StatusFunction)();

typedef struct Plugin {
    void *so;
    char format;
    InitFunction init;
    ExitFunction exit;
    StatusFunction status;
} Plugin;

static Display *dpy;
static Window root;
static Plugin *plugins[NPLUGINS];

static bool init_display();
static Plugin *init_plugin(char *plugin);
static bool init_plugins();
static void clean_display();
static void clean_plugins();

static const char *get_status();
static const char *get_plugin_status(char format);

int
main(int argc, const char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    if (!init_display() || !init_plugins()) {
        return 1;
    }

    while (true) {
        XStoreName(dpy, root, get_status());
        XFlush(dpy);
        sleep(1);
    }

    return 0;
}

bool
init_display()
{
    openlog("dstatus", 0, LOG_USER);

    dpy = XOpenDisplay(0);
    if (dpy == 0) {
        syslog(LOG_ERR, "error initializing display");
        return false;
    }

    root = DefaultRootWindow(dpy);
    if (root == 0){
        XCloseDisplay(dpy);
        syslog(LOG_ERR, "error initializing root window");
        return false;
    }

    atexit(clean_display);

    return true;
}

Plugin *
init_plugin(char *name)
{
    void *so = dlopen(name, RTLD_NOW);
    if (so == NULL) {
        syslog(LOG_ERR, dlerror());
        return NULL;
    }

    InitFunction init = dlsym(so, "plugin_init");
    ExitFunction exit = dlsym(so, "plugin_exit");
    StatusFunction status = dlsym(so, "plugin_status");
    FormatFunction format = dlsym(so, "plugin_format");

    if (init == NULL || exit == NULL || status == NULL || format == NULL) {
        syslog(LOG_ERR, dlerror());
        dlclose(so);
        return NULL;
    }

    Plugin *plugin = malloc(sizeof(Plugin));
    plugin->so = so;
    plugin->init = init;
    plugin->exit = exit;
    plugin->status = status;
    plugin->format = format();

    return plugin;
}

bool
init_plugins()
{
    DIR *soDir = opendir(SOPATH);
    if (soDir == NULL) {
        syslog(LOG_ERR, "plugin directory " SOPATH " does not exist");
        return false;
    }

    atexit(clean_plugins);

    char path[128];
    struct dirent *file;
    int i = 0;

    for (file = readdir(soDir); i < NPLUGINS && file != NULL; file = readdir(soDir)) {
        if (strstr(file->d_name, ".so") == NULL) {
            continue;
        }

        snprintf(path, 128, "%s%s", SOPATH, file->d_name);

        Plugin *plugin = init_plugin(path);
        if (plugin != NULL) {
            plugin->init();
            plugins[i++] = plugin;
        }
    }

    closedir(soDir);

    return true;
}

void
clean_display()
{
    XCloseDisplay(dpy);
    closelog();
}

void
clean_plugins()
{
    Plugin *plugin;
    int i;

    for (i = 0, plugin = plugins[i]; i < NPLUGINS && plugin != 0; plugin = plugins[++i]) {
        dlclose(plugin->so);
    }
}

const char *
get_status()
{
    static const char *format = FORMAT;
    static char buf[STATUS_LENGTH];

    int formatlen = strlen(format);
    int i, j, escaped = 0;
    int groupstart = -1;

    buf[0] = '\0';

    for (i = 0, j = 0; i < formatlen && j < STATUS_LENGTH - 1; i++) {
        char c = format[i];

        if (!escaped) {
            if (c == '%') {
                escaped = 1;
            } else {
                buf[j++] = c;
            }
        } else {
            if (c == '%') {
                buf[j++] = '%';
            } else if (c == '(' && groupstart == -1) {
                groupstart = j;
            } else if (c == ')') {
                groupstart = -1;
            } else {
                const char *status = get_plugin_status(c);

                if (status != NULL) {
                    strncpy(&buf[j], status, STATUS_LENGTH - j);
                    j = j + strlen(status);
                } else if (groupstart >= 0) {
                    i = strstr(format + i, "%)") + 1 - format;
                    j = groupstart;

                    buf[j] = '\0';
                    groupstart = -1;
                }
            }

            escaped = 0;
        }
    }

    buf[j] = '\0';

    return buf;
}

const char *
get_plugin_status(char format)
{
    if (format == '%') {
        return "%";
    }

    Plugin *plugin;
    int i;

    for (i = 0, plugin = plugins[i]; i < NPLUGINS && plugin != NULL; plugin = plugins[++i]) {
        if (plugin->format == format) {
            return plugin->status();
        }
    }

    return NULL;
}
