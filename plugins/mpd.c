/*
 * Copyright (c) 2012-2013, Patrick Steinhardt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>
#include <stdio.h>
#include <syslog.h>
#include <mpd/client.h>

#include "../config.h"

#define LENGTH(x)               (sizeof(x) / sizeof(x[0]))

struct TagTypeFormat {
    char format;
    enum mpd_tag_type tag;
};

static const char *parse_song(struct mpd_song *song);
static const char *format = MPD_FORMAT;
static struct mpd_connection *mpdConnection = NULL;
static struct TagTypeFormat tags[] = {
    { 'a', MPD_TAG_ARTIST },
    { 'b', MPD_TAG_ALBUM },
    { 'd', MPD_TAG_DISC },
    { 'g', MPD_TAG_GENRE },
    { 'n', MPD_TAG_TRACK },
    { 'p', MPD_TAG_COMPOSER },
    { 't', MPD_TAG_TITLE },
    { 'y', MPD_TAG_DATE },
    { 'z', MPD_TAG_ALBUM_ARTIST },
};

bool
plugin_init()
{
    mpdConnection = mpd_connection_new(0, 0, 0);
    if (mpd_connection_get_error(mpdConnection) != MPD_ERROR_SUCCESS) {
        syslog(LOG_ERR, "error initializing MPD");
        return false;
    }

    return true;
}

void
plugin_exit()
{
    mpd_connection_free(mpdConnection);
}


const char *
plugin_status()
{
    const char *buf = NULL;
    struct mpd_status *status = NULL;
    struct mpd_song *song = NULL;
    enum mpd_state state;
    int songId;

    status = mpd_run_status(mpdConnection);
    if (status == 0)
        goto out;

    state = mpd_status_get_state(status);
    if (state != MPD_STATE_PLAY)
        goto out;

    songId = mpd_status_get_song_id(status);
    song = mpd_run_get_queue_song_id(mpdConnection, songId);
    if (song == 0)
        goto out;

    buf = parse_song(song);

out:
    if (status)
        mpd_status_free(status);
    if (song)
        mpd_song_free(song);

    return buf;
 }

char
plugin_format()
{
    return 'm';
}

const char *
parse_song(struct mpd_song *song)
{
    static char buf[MPD_LEN];
    int formatlen = strlen(format);
    bool escaped = false;
    int i, j;

    for (i = 0, j = 0; i < formatlen && j < MPD_LEN - 1; i++) {
        char c = format[i];

        if (!escaped) {
            if (c == '%') {
                escaped = true;
            } else {
                buf[j++] = format[i];
            }
        } else if (c == '%') {
            buf[j++] = '%';
            escaped = false;
        } else {
            const char *tag;
            enum mpd_tag_type tagType = -1;
            unsigned int k;
            int taglen;

            for (k = 0; k < LENGTH(tags); k++) {
                if (tags[k].format == c) {
                    tagType = tags[k].tag;
                    break;
                }
            }

            tag = mpd_song_get_tag(song, tagType, 0);
            if (tag == NULL) {
                continue;
            }

            taglen = strlen(tag);
            if (taglen > MPD_LEN - j) {
                taglen = MPD_LEN - j;
            }

            memcpy(buf + j, tag, taglen);
            j += taglen;
            escaped = false;
        }
    }

    buf[j] = '\0';

    return buf;
}
